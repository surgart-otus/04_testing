#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
import datetime
import re
import logging
import hashlib
import uuid
from optparse import OptionParser
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler


import scoring

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Field(object):
    def __init__(self, required, nullable, field_name=None):
        self.required = required
        self.nullable = nullable
        self.field_name = field_name
        self.value = None

    def __set__(self, instance, value):
        self.value = value
        self._validate()

    def __get__(self, instance, cls):
        return self.value

    def _validate(self):
        if self.required and self.value is None:
            message = 'Field "%s" is required' % self.field_name
            logging.error(message)
            raise ValueError(message)

        if not self.nullable and len(self.value) == 0:
            message = 'Field "%s" can not be empty' % self.field_name
            logging.error(message)
            raise ValueError(message)


class CharField(Field):
    def __add__(self, other):
        if(isinstance(other, str)):
            self.value = ''.join([self.value, other])
            return self.value

        self.value = ''.join([self.value, other.value])
        return self

    def _validate(self):
        super(CharField, self)._validate()
        if (not isinstance(self.value, str) and not isinstance(self.value, unicode)) and self.value is not None:
            message = 'Field "%s" is invalid. Expected string got "%s". Value = "%s"' % (self.field_name, type(self.value), self.value)
            logging.error(message)
            raise ValueError(message)


class ArgumentsField(Field):
    def _validate(self):
        super(ArgumentsField, self)._validate()
        if not isinstance(self.value, dict):
            message = 'Field "%s" is invalid. Expected dictionary got "%s"' % (self.field_name, type(self.value))


class EmailField(CharField):
    def _validate(self):
        super(EmailField, self)._validate()
        pattern = re.compile(r'\w+[\w.-]@[\w.-]*[\w]+')
        if self.value is not None:
            match = re.match(pattern, self.value)
            if not match:
                message = 'Field "%s" is invalid email' % self.field_name
                logging.error(message)
                raise ValueError(message)


class PhoneField(Field):
    def __set__(self, instance, value):
        if isinstance(value, long):
            self.value = str(value)
        else:
            self.value = value
        self._validate()

    def _validate(self):
        super(PhoneField, self)._validate()
        if self.value is not None:
            if not isinstance(self.value, str):
                message = 'Field "%s" is invalid phone number. Expected "str" got "%s".' % (self.field_name, type(self.value))
                logging.error(message)
                raise ValueError(message)

            pattern = re.compile(r'7\d{10}')
            match = re.match(pattern, self.value)
            if not match:
                message = 'Field "%s" is invalid phone number. It must start with 7 and contain 11 digits.' % self.field_name
                logging.error(message)
                raise ValueError(message)


class DateField(CharField):
    def _validate(self):
        super(DateField, self)._validate()
        try:
            if self.value is not None:
                datetime.datetime.strptime(self.value, '%d.%m.%Y')
        except ValueError:
            message = 'Field "%s" is invalid date or has invalid format. It must have DD.MM.YYYY format' % self.field_name
            logging.error(message)
            raise ValueError(message)


class BirthDayField(DateField):
    def _validate(self):
        super(BirthDayField, self)._validate()
        if self.value is not None:
            birthday = datetime.datetime.strptime(self.value, '%d.%m.%Y')
            today = datetime.datetime.today()
            delta = datetime.timedelta(days=365*70)
            if(today - birthday > delta):
                message = 'Field "%s" is invalid birthday. It is older than 70 years' % self.field_name
                logging.error(message)
                raise ValueError(message)


class GenderField(Field):
    def _validate(self):
        super(GenderField, self)._validate()
        if self.value not in (UNKNOWN, MALE, FEMALE) and self.value is not None:
            message = 'Field "%s" must be 0, 1, 2' % self.field_name
            logging.error(message)
            raise ValueError(message)


class ClientIDsField(Field):
    def __init__(self, required, field_name=None):
        super(ClientIDsField, self).__init__(required, True, field_name)
        self.value = []

    def _validate(self):
        super(ClientIDsField, self)._validate()
        if not isinstance(self.value, list):
            message = 'Field "%s" is invalid. It must be a list' % self.field_name
            logging.error(message)
            raise ValueError(message)

        if not all([isinstance(el, int) for el in self.value]):
            message = 'Field "%s" is invalid. It must contain integer values' % self.field_name
            logging.error(message)
            raise ValueError(message)

        if self.required and len(self.value) == 0:
            message = 'Field "%s" is invalid. It is required' % self.field_name
            logging.error(message)
            raise ValueError(message)


class Request(object):

    @property
    def arguments(self):
        return self.arguments

    @arguments.setter
    def arguments(self, arguments):
        for field in self.fields_list:
            setattr(self, field, arguments.get(field))
        self._validate()

    def _validate(self):
        pass


class ClientsInterestsRequest(Request):
    fields_list = ['client_ids', 'date']

    client_ids = ClientIDsField(required=True, field_name='client_ids')
    date = DateField(required=False, nullable=True, field_name='date')

    @property
    def context(self):
        return len(self.client_ids)


class OnlineScoreRequest(Request):
    fields_list = ['first_name', 'last_name', 'email', 'phone', 'birthday', 'gender']

    first_name = CharField(required=False, nullable=True, field_name='first_name')
    last_name = CharField(required=False, nullable=True, field_name='last_name')
    email = EmailField(required=False, nullable=True, field_name='email')
    phone = PhoneField(required=False, nullable=True, field_name='phone')
    birthday = BirthDayField(required=False, nullable=True, field_name='birthday')
    gender = GenderField(required=False, nullable=True, field_name='gender')

    def _validate(self):
        super(OnlineScoreRequest, self)._validate()

        def _len(value):
            return len(value) if value is not None else 0

        len_first_name = _len(self.first_name)
        len_last_name = _len(self.last_name)
        len_email = _len(self.email)
        len_phone = _len(self.phone)
        len_birthday = _len(self.birthday)
        if not(len_first_name > 0 and len_last_name > 0 or
            len_phone > 0 and len_email > 0 or
            len_birthday > 0 and self.gender is not None):
            message = 'At least one of couples of arguments (phone-email, first_name-last_name, birthday-gender) must be filled'
            logging.error(message)            
            raise ValueError(message)

    @property
    def context(self):
        ctx = []
        for field in self.fields_list:
            value = getattr(self, field)
            if value is not None:
                ctx.append(field)
        return ctx


class MethodRequest(object):
    account = CharField(required=False, nullable=True, field_name='account')
    login = CharField(required=True, nullable=True, field_name='login')
    token = CharField(required=True, nullable=True, field_name='token')
    __arguments = ArgumentsField(required=True, nullable=True, field_name='arguments')
    __method = CharField(required=True, nullable=False, field_name='method')

    def __init__(self):
        self.request = None

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN

    @property
    def method(self):
        return self.__method

    @method.setter
    def method(self, value):
        self.__method = value
        if self.__method == 'online_score':
            self.request = OnlineScoreRequest()
        
        elif self.__method == 'clients_interests':
            self.request = ClientsInterestsRequest()
            
        else:
            raise ValueError('Invalid method request')

    @property
    def arguments(self):
        return self.__arguments

    @arguments.setter
    def arguments(self, args):
        if not isinstance(args, dict):
            raise ValueError('Aguments must be dictionary')

        self.__arguments = args
        self.request.arguments = args

    def send_request(self, store):
        if self.__method == 'online_score':
            score = scoring.get_score(store=store,
                        phone=self.request.phone,
                        email=self.request.email,
                        birthday=self.request.birthday,
                        gender=self.request.gender,
                        first_name=self.request.first_name,
                        last_name=self.request.last_name)
            response, code = {'score': score}, OK
            
        elif self.__method == 'clients_interests':
            response, code = {}, OK
            for client_id in self.request.client_ids:
                interests = scoring.get_interests(store=store, cid=self.request.client_ids)
                response[client_id] = interests

        else:
            logging.error(ERRORS[INVALID_REQUEST])
            response, code = {'code': INVALID_REQUEST, 'error': ERRORS[INVALID_REQUEST]}, INVALID_REQUEST

        return response, code
    
    @property
    def context(self):
        if self.__method == 'online_score':
            return {'has': self.request.context}

        elif self.__method == 'clients_interests':
            return {'nclients': self.request.context}

        else:
            raise ValueError('Invalid method request "%s"' % self.method)


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(request.account + request.login + SALT).hexdigest()

    if digest == request.token:
        return True
    return False


def method_handler(request, ctx, store):
    response, code = None, None
    body = request.get('body')
    method_request = MethodRequest()
    try:
        method_request.method = body.get('method')

    except ValueError:
        response, code = {'code': INVALID_REQUEST, 'error': ERRORS[INVALID_REQUEST]}, INVALID_REQUEST
        return response, code

    try:
        method_request.login = body.get('login')
        method_request.account = body.get('account')
        method_request.token = body.get('token')
    except ValueError:
        response, code = {'code': FORBIDDEN, 'error': ERRORS[FORBIDDEN]}, FORBIDDEN
        logging.error()
        return response, code

    if not check_auth(method_request):
        response, code = {'code': FORBIDDEN, 'error': ERRORS[FORBIDDEN]}, FORBIDDEN
        return response, code

    try:
        method_request.arguments = body.get('arguments')
        ctx.update(method_request.context)
        body.get('arguments')

    except ValueError:
        logging.error(ERRORS[INVALID_REQUEST])
        response, code = {'code': INVALID_REQUEST, 'error': ERRORS[INVALID_REQUEST]}, INVALID_REQUEST
        return response, code

    if method_request.login == 'admin':
        response, code = {'score': 42}, OK
    else:
        ctx = method_request.context
        response, code = method_request.send_request(store)

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


def set_logging(level, filename=None):
    logging.basicConfig(filename=filename, level=level,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    set_logging(logging.INFO, opts.log)    
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
