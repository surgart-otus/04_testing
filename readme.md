# Scoring API
> Provide web API to get score or interests of clients

### Requirements

This application requires python 2.7

### Launching application
```bash
$python apy.py --port 8080 --log log.txt
```
It starts http server listening on port 8080 (by default 8080) and writes log in log.txt file.

### Running Tests
```bash
$python test.py
```

### Usage examples

#### Example 1:
```bash
$ curl -X POST -H "Content-Type: application/json" -d '{"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95", "arguments": {"phone": "79175002040", "email": "stupnikov@otus.ru", "first_name": "Станислав", "last_name": "Ступников", "birthday": "01.01.1990", "gender": 1}}'
    http://127.0.0.1:8080/method/
```

#### Example 2:
```javascript
// javascript
request = {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95", "arguments": {"phone": "79175002040", "email": "stupnikov@otus.ru", "first_name": "Станислав", "last_name": "Ступников", "birthday": "01.01.1990", "gender": 1}};

$.post(url="http://localhost:8080/method/", data=JSON.stringify(request), dataType="json")
.success(function(data){
    console.log(data);
})
.fail(function(data){
    console.log(data);
});
```